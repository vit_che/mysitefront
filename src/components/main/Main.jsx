import React from 'react'
import Carusel from './Carusel'
import Content from "./Content"

class Main extends React.Component {

    render(){

        return (
            <div className="container">

                <Carusel/>
                <Content/>

            </div>
        )
    }
}

export default Main;