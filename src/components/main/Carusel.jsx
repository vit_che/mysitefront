import React from 'react'
import '../../css/nav.css'
import pic from "./../../img/slide1.jpg"

class Carusel extends React.Component {
    render(){

        return (
            <div className="nav container">

                {/*<img className="d-block w-100 h-100" src={pic} alt="First slide"/>*/}
                <img className="container" src={pic} alt="First slide"/>

            </div>
        )
    }
}

export default Carusel;