import React from 'react'
import '../../css/header.css'

class Header extends React.Component {
    render(){

        return (
            <div className="header container">
                <div>
                    <p className="title text-center">my personal site</p>
                </div>
            </div>
        )
    }
}

export default Header;