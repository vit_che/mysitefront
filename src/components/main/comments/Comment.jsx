import React from 'react'

class Comment extends React.Component {

    constructor(props){
        super(props)
        this.state = {

        }
    }

    render(){

        const { data } = this.props

        return (
            <div className="comment container">
                <div className='card'>
                    <div className="card-body">
                        <p className="card-title text-center"> {data.title}</p>
                        <p className="card-text">{data.text}</p>
                        <p className="">Author: {data.name}</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Comment;