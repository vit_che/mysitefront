import React from 'react'
import axios from 'axios'


class CommentForm extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            title: '',
            text: ''
        }
    }

    handleChangeTitle = event => {
        this.setState({ title: event.target.value });
    }
    handleChangeComment = event => {
        this.setState({ text: event.target.value });
    }
    handleSubmit = event => {
        event.preventDefault();


        const comListTrue = this.props.comListTrue
        const comListFalse = this.props.comListFalse 

        const onButton = this.props.comFormClose
        const text = this.state.text
        const title = this.state.title  
        axios.post('http://mysite/addComment/',{ title, text })
            .then(res => {
                console.log(res.data);

                comListFalse();
                comListTrue();
                onButton();


            })
    }

    render(){

        return (
            <div className="content container">

                <div className='formcomment'>

                    <form onSubmit={this.handleSubmit}>

                        <div className="form-group">
                            <input type="text" name="title" className="form-control" id="InputTitle" onChange={this.handleChangeTitle} placeholder="Input Title"/>
                        </div>

                        <div className="form-group">
                            <textarea type="text" name="text" className="form-control" id="InputComment" onChange={this.handleChangeComment} />
                            <small id="CommentHelp" className="form-text text-muted"> Add here your comment</small>
                        </div>

                        <button type="submit"   className="btn btn-secondary btn-sm">Submit</button>
                        <button className="btn btn-light btn-sm" onClick={ this.props.commentsFormClose }>Cancel</button>

                    </form>

                </div>

            </div>
        )
    }
}

export default CommentForm;