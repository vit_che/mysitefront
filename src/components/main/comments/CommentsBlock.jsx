import React from 'react'
import CommentsList from "./CommentsList"
import CommentForm from "./CommentForm";



class CommentsBlock extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            isOpen: false,
            isOpn: false,
            openComList: true
        }
    }

    render(){

        // const { data } = this.props

        const commentsForm = this.state.isOpen && <div id="formcomments">
            <CommentForm  comFormClose={this.handleCommentsForm}
                          comListTrue={this.handleComListTrue}
                          comListFalse={this.handleComListFalse} />
        </div>

        const commentsFormButton = <button type="button" className="btn btn-secondary"
               onClick={ this.handleCommentsForm }>
            { !this.state.isOpen ? 'Open form input feedback' : 'Close Form' }
        </button>

        const commentList =  this.state.openComList && <CommentsList  />



        return (
            <div className="comment container">

                <h3>CommentsBlock</h3>

                { commentsFormButton }

                { commentsForm }

                { commentList }

                {/* <CommentsList /> */}

            </div>
        )
    }

    handleCommentsForm = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    handleComListTrue = () => {
        this.setState({
            openComList: true
        })
    }
    handleComListFalse = () => {
        this.setState({
            openComList: false
        })
    }


}

export default CommentsBlock;