import React from 'react'
import Comment from './Comment'
import axios from 'axios'


class CommentsList extends React.Component {

    state = {
        comment: []
    }

    componentDidMount(){

        this.getComments()
    }

    render(){

        const commentElem = this.state.comment.map(data =>

            <div key = { data.id }>

                <Comment data = { data } />

            </div>)

        return (
            <div className="commentslist container">

                <h5>Comments List</h5>

                { commentElem }

            </div>
        )
    }

    getComments = () => {
        axios.get('http://mysite/getComments/')
        .then(res => {
            const comment = res.data;
            console.log("comment : " + res.data)
            this.setState({ comment })
        })
    }
}

export default CommentsList;