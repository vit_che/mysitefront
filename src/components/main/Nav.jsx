import React from 'react'
import '../../css/nav.css'
import { Link } from 'react-router-dom'

class Nav extends React.Component {


    render(){

        return (
            <div className="nav container nopd">

                <nav className="container-fluid navbar navbar-expand-lg navbar-light bg-light">

                    <a className="navbar-brand font" href="#">
                        CheCom
                    </a>

                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav red">
                            <li className="nav-item">
                                {/*<a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>*/}
                                <Link to='/'>Home</Link>
                            </li>
                            <li className="nav-item">
                                {/*<a className="nav-link" href="#">Feedbacks</a>*/}
                                <Link to='/comments'>FeedBacks</Link>

                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Pricing</a>
                            </li>
                        </ul>
                    </div>
                </nav>



            </div>
        )
    }
}

export default Nav;