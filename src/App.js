import React, { Component } from 'react'
import './App.css'
import 'bootstrap/dist/css/bootstrap.css'
import Header from './components/main/Header'
import Nav from './components/main/Nav'
import Main from './components/main/Main'
import CommentsBlock from "./components/main/comments/CommentsBlock"


import { BrowserRouter, Route, Switch } from 'react-router-dom'


class App extends Component {

  render() {

    return (

      <div className="App">

          <Header />

          <BrowserRouter>

              <div>

                  <Nav />

                  <Route>

                      <Switch>

                          <Route exact path='/' component={Main}/>

                          <Route path='/comments' component={CommentsBlock}/>                      </Switch>

                  </Route>

              </div>

          </BrowserRouter>

      </div>
    );
  }
}

export default App;
